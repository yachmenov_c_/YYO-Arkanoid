/**
 * Created by yaroslav on 15.05.16.
 */
/*
 ------
 Blocks class
 ------
 */

function YYO_Blocks(rows_blocks, cols_blocks){
    var count = rows_blocks * cols_blocks;
    var rows = rows_blocks;
    var cols = cols_blocks;
    var percent_area = 40;
    var block_width = this.getWidth() / cols;
    var block_height = this.getHeight()*(percent_area/100) / rows;

    this.wasExpandBlocks = false;
    this.expandBlocksTimer = 0;

    this.wasCatchBlocks = false;
    this.catchBlocksTimer = 0;

    this.case = new Array(rows);
    for(var i = 0; i < rows; i++){
        this.case[i] = new Array(cols);
        for(var j = 0; j < cols; j++){
            var type = 'simple';
            if(i === 1){
                type = 'expand';
            }
            if((i === 2 || i === 3) && ( j === 2 || j === cols - 3 )){
                type = 'catch';
            }

            /**
             * @type YYO_Block
             */
            this.case[i][j] = new YYO_Block(block_width * j, block_height * i, block_width, block_height, this.context, type);
            this.case[i][j].draw();
        }
    }

    this.draw_blocks = function(){
        for(var i = 0; i < rows; i++){
            for(var j = 0; j < cols; j++){
                this.case[i][j].draw();
            }
        }
    };

    this.expandBlocksAction = function(){
        if(!this.wasExpandBlocks){
            this.wasExpandBlocks = true;
            this.rocket.x_rock -= this.rocket.width;
            this.rocket.width *= 2;
            this.expandBlocksTimer = setTimeout(function(){
                this.wasExpandBlocks = false;
                this.rocket.width /= 2;
                this.rocket.x_rock += this.rocket.width;
                clearTimeout(this.expandBlocksTimer);
            }.bind(this), 1000 * 10);
        }
    };

    this.catchBlocksAction = function(){
        if(!this.wasCatchBlocks){
            var timer = this.ball.timer_flyID;
            this.ball.timer_flyID = 0;
            clearTimeout(timer);
            this.wasCatchBlocks = true;
            this.ball.was_started = false;
            setTimeout(function(){
                this.ball.x_ball = this.ball.ballXFromRocket();
                this.ball.y_ball = this.ball.ballYFromRocket();
                this.rocket.draw_rocket();
            }.bind(this), this.ball.fly_miliseconds);
            this.catchBlocksTimer = setTimeout(function(){
                this.wasCatchBlocks = false;
                this.ball.fly();
                clearTimeout(this.catchBlocksTimer);
            }.bind(this), 1000 * 10);
        }
    };

    return this;
}

/**
 *
 * @param {Number} x_coord
 * @param {Number} y_coord
 * @param {Number} w
 * @param {Number} h
 * @param {CanvasRenderingContext2D} canvas_context
 * @param {String} type_block
 * @constructor
 */
function YYO_Block(x_coord, y_coord, w, h, canvas_context, type_block){
    var type = type_block;

    var color;
    var color_stroke;
    var width = w;
    var height = h;
    var x = x_coord;
    var y = y_coord;
    var canvas = canvas_context;
    var was_destroyed = false;

    switch(type_block){
        case 'expand':
            color = 'rgba(0, 0, 255, 0.5)';
            color_stroke = 'yellow';
            break;
        case 'catch':
            color = 'rgba(0, 255, 0, 0.5)';
            color_stroke = 'aqua';
            break;
        default:
            type = 'simple';
            color = 'rgba(255, 255, 0, 0.5)';
            color_stroke = 'blue';
            break;
    }

    this.setStyles = function(){
        canvas.fillStyle = color;
        canvas.strokeStyle = color_stroke;
        canvas.shadowColor = 'grey';
        canvas.shadowBlur = 1;
        canvas.lineJoin="miter";
    };

    this.draw = function(){
        if(!was_destroyed) {
            canvas.save();
            this.setStyles();
            canvas.beginPath();
            canvas.rect(x, y, width, height);
            canvas.fill();
            canvas.stroke();
            canvas.closePath();
            canvas.restore();
        }
    };

    this.isPointInside = function(x_coord, y_coord){
        if(was_destroyed) return false;
        var inPath = false;
        canvas.beginPath();
        canvas.rect(x, y, width, height);
        if(canvas.isPointInPath(x_coord, y_coord)){
            inPath = true;
        }
        canvas.closePath();
        return inPath;
    };

    this.selfClear = function(){
        canvas.clearRect(x, y, width, height);
        was_destroyed = true;
    };

    this.getHeight = function(){ return height;};
    this.getWidth = function(){ return width;};
    this.getX = function(){ return x;};
    this.getY = function(){ return y;};
    this.isDestroyed = function(){ return was_destroyed;};
    this.getType = function(){ return type;};
}