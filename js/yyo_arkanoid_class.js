/**
 * Created by yaroslav on 14.05.16.
 */

/*
 ------
 Arkanoid class
 ------
 */

function YYO_Arkanoid(canvas_content) {
    this.canvas_content = canvas_content;
    this.context = this.canvas_content.getContext('2d');
    /**
     * @type YYO_Blocks
     */
    this.blocks = YYO_Blocks.call(this, 5, 10);
    /**
     * @type YYO_Rocket
     */
    this.rocket = YYO_Rocket.call(this);
    /**
     * @type YYO_Ball
     */
    this.ball = YYO_Ball.call(this);
    this.rocket.draw_rocket();
}

YYO_Arkanoid.prototype.getWidth = function () {
    return parseInt(this.canvas_content.width);
};

YYO_Arkanoid.prototype.getHeight = function () {
    return parseInt(this.canvas_content.height);
};

YYO_Arkanoid.prototype.bulletXY = function(mainX, mainY, bulletX, bulletY, rotation) {
    return {
        x: Math.cos(rotation) * (bulletX - mainX) - Math.sin(rotation) * (bulletY - mainY) + mainX,
        y: Math.sin(rotation) * (bulletX - mainX) + Math.cos(rotation) * (bulletY - mainY) + mainY
    };
};

YYO_Arkanoid.prototype.clearScreen = function(x, y, w, h){
    if (x === undefined) x = 0;
    if (y === undefined) y = 0;
    if (w === undefined) w = this.getWidth();
    if (h === undefined) h = this.getHeight();

    this.context.clearRect(x, y, w, h);
};