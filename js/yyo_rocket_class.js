/**
 * Created by yaroslav on 14.05.16.
 */
/*
 ------
 Rocket class
 ------
 */
function YYO_Rocket(){
    this.height = 50;
    this.width = 125;
    this.step = 10;
    this.angle = 0.02;
    this.go_miliseconds = 30;
    this.back_miliseconds = 5;
    this.transformed_coords = {};

    var color = 'green';
    var color_stroke = 'lightblue';

    this.x_rock = this.getWidth()/2 - this.width/2;
    this.y_rock = this.getHeight() - this.height - 100;

    this.draw_rocket = function(x, y, width, height) {

        this.clearScreen(this.x_rock, this.y_rock, this.width, this.height);
        if(!this.ball.was_started && !this.rocket.rotate_timerID){
            this.ball.draw_ball();
            this.blocks.draw_blocks();
        }

        this.context.save();

        if (x === undefined) x = this.x_rock;
        if (y === undefined) y = this.y_rock;
        if (width === undefined) width = this.width;
        if (height === undefined) height = this.height;

        this.rectStylesSet();

        this.context.fillRect(x, y, width, height);
        this.context.strokeRect(x, y, this.width, this.height);
        this.context.restore();

    };

    this.actionGoToRight = function(){
        if(!this.rotate_timerID){
            this.context.clearRect(this.x_rock, this.y_rock, this.width, this.height);
            var condition = function(that){ return (that.x_rock + that.width) <= that.getWidth() };
            this.rotate_timerID = setInterval(this.timerGoRotate.bind(this, condition, this.angle, this.step), this.go_miliseconds);
        }
    };

    this.actionEndToRight = function(){
        clearInterval(this.rotate_timerID);
        this.rotate_timerID = setInterval(this.timerGoBack.bind(this, this.angle), this.back_miliseconds);
    };

    this.actionGoToLeft = function(){
        if(!this.rotate_timerID){
            this.context.clearRect(this.x_rock, this.y_rock, this.width, this.height);
            var condition = function(that){ return that.x_rock >= 0 };
            this.rotate_timerID = setInterval(this.timerGoRotate.bind(this, condition, -this.angle, -this.step), this.go_miliseconds);
        }
    };

    this.actionEndToLeft = function(){
        clearInterval(this.rotate_timerID);
        this.rotate_timerID = setInterval(this.timerGoBack.bind(this, -this.angle), this.back_miliseconds);
    };

    this.rotate_timerID = 0;
    this.rotate_count = 0;

    this.timerGoRotate = function(condition, rotate_count, x_step){
        if(condition(this)) {
            this.rotateClearRect();
            this.rotate_count += rotate_count;
            this.rotateFillRect();
            this.x_rock += x_step;
            if(!this.ball.was_started) this.ball.x_ball += x_step;
        }
    };

    this.timerGoBack = function(rotate_count){
        if((rotate_count > 0 && this.rotate_count > 0) || (rotate_count < 0 && this.rotate_count < 0)) {
            this.rotateClearRect();
            this.rotate_count -= rotate_count;
            this.rotateFillRect();
        } else {
            var id = this.rotate_timerID;
            this.rotate_timerID = 0;
            this.rotateClearRect();
            this.draw_rocket();
            clearInterval(id);
        }
    };

    this.rotateClearRect = function(){
        this.context.save();
        this.rectStylesSet();
        this.context.translate(this.x_rock - this.step, this.y_rock);
        this.context.rotate(Math.PI/180 + this.rotate_count);
        if(!this.ball.was_started){
            this.context.clearRect(-50, -50, this.width + 100, this.height + 100);
        } else {
            this.context.clearRect(-2, -2, this.width + 50, this.height + 50);
        }
        this.context.restore();
    };

    this.rotateFillRect = function(){
        this.context.save();
        this.rectStylesSet();
        this.context.translate(this.x_rock, this.y_rock);
        this.context.rotate(Math.PI/180 + this.rotate_count );
        this.context.fillRect(0, 0, this.width, this.height);
        this.context.strokeRect(0, 0, this.width, this.height);
        this.transformed_coords = this.bulletXY(this.rocket.x_rock, this.rocket.y_rock, this.rocket.x_rock + this.rocket.width/2, this.rocket.y_rock, this.rotate_count);
        if(!this.ball.was_started){
            this.ball.onlyDrawBall(this.width / 2, -this.radius, this.ball.radius);
            var coords_ball = this.bulletXY(this.rocket.x_rock, this.rocket.y_rock, this.ballXFromRocket(), this.ballYFromRocket(), this.rotate_count);
            var len = Math.sqrt(Math.pow(coords_ball.x - this.transformed_coords.x, 2) + Math.pow(coords_ball.y - this.transformed_coords.y, 2));
            this.ball.changeVector((coords_ball.x - this.transformed_coords.x ) / len * this.ball.radius, (coords_ball.y - this.transformed_coords.y) / len * this.ball.radius);
            this.ball.x_ball = coords_ball.x;
            this.ball.y_ball = coords_ball.y;
        }
        this.context.restore();
    };

    this.rectStylesSet = function(){
        this.context.strokeStyle = color_stroke;
        this.context.fillStyle = color;
        var grd = this.context.createLinearGradient(this.x_rock, this.y_rock, this.width, this.height);
    };

    return this;
}
