/**
 * Created by yaroslav on 14.05.16.
 */
document.addEventListener("DOMContentLoaded", yyo_arkanoid_init);

function yyo_arkanoid_init(){
    var game = new YYO_Arkanoid(document.getElementById('yyo-arkanoid'));

    document.addEventListener("keydown", function(event){
        var keyCode = ('which' in event) ? event.which : event.keyCode;
        switch(keyCode){
            case 37:
                /* LEFT */
                game.rocket.actionGoToLeft();
                break;
            case 39:
                /* RIGHT */
                game.rocket.actionGoToRight();
                break;
        }
    });

    document.addEventListener("keyup", function(event){
        var keyCode = ('which' in event) ? event.which : event.keyCode;
        switch(keyCode){
            case 37:
                /* LEFT */
                game.rocket.actionEndToLeft();
                break;
            case 39:
                /* RIGHT */
                game.rocket.actionEndToRight();
                break;
            case 32:
                /* SPACE BAR */
                game.ball.fly();
                break;
        }
    });
}