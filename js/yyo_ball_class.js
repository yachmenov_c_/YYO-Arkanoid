/**
 * Created by yaroslav on 14.05.16.
 */
/*
 ------
 Ball class
 ------
 */

function YYO_Ball(){
    this.ballXFromRocket = function(){ return this.rocket.x_rock + this.rocket.width / 2; };
    this.ballYFromRocket = function(){ return this.rocket.y_rock - this.radius; };

    this.radius = 10;
    this.x_ball = this.ballXFromRocket();
    this.y_ball = this.ballYFromRocket();

    this.was_started = false;
    var color = 'aqua';
    var color_stroke = 'white';
    var vector = {
        x: 0,
        y: -this.radius
    };
    this.fly_miliseconds = 30;
    this.vector_prod = 1;

    this.ballXNext = function(){ return this.x_ball + vector.x * this.vector_prod; };
    this.ballYNext = function(){ return this.y_ball + vector.y * this.vector_prod; };


    this.draw_ball = function(x, y, radius){

        this.context.save();
        this.ballStylesSet();
        this.clearScreen();

        if(this.ball.was_started){
            if(!this.rocket.rotate_timerID) {
                this.rocket.draw_rocket();
            } else {
                this.rocket.rotateClearRect();
                this.rocket.rotateFillRect();
            }

            this.blocks.draw_blocks();
        }

        if (x === undefined) x = this.x_ball;
        else this.x_ball = x;
        if (y === undefined) y = this.y_ball;
        else this.y_ball = y;
        if (radius === undefined) radius = this.radius;
        else this.radius = radius;

        this.context.beginPath();
        this.context.arc(x, y, radius, 0, Math.PI*2);
        this.context.fill();
        this.context.stroke();
        this.context.closePath();

        this.context.restore();

    };

    this.ballStylesSet = function(){
        this.context.fillStyle = color;
        this.context.strokeStyle = color_stroke;
    };

    this.onlyDrawBall = function(x, y, radius){
        this.ballStylesSet();

        this.context.beginPath();
        this.context.arc(x, y, radius, 0, Math.PI*2);
        this.context.fill();
        this.context.stroke();
        this.context.closePath();

    };

    this.fly = function(){
        if(!this.was_started){
            if(this.blocks.wasCatchBlocks){
                this.blocks.wasCatchBlocks = false;
                clearTimeout(this.blocks.catchBlocksTimer);
            }
            this.was_started = true;
            this.timer_flyID = setInterval(function(){
                this.flyIsWall();
                this.flyIsRocket();
                this.flyIsRotateRocket();
                this.flyIsBlocks();
                this.draw_ball(this.x_ball + vector.x*this.vector_prod, this.y_ball + vector.y * this.vector_prod);
            }.bind(this), this.fly_miliseconds);
        }
    };

    this.changeVector = function(x, y){
        vector.x = x;
        vector.y = y;
    };

    this.flyIsWall = function(){
        var new_x = this.ballXNext();
        var new_y = this.ballYNext();
        if( new_y <= 0 ) vector.y *= -1;
        if( new_x >= this.getWidth() ) vector.x *= -1;
        if( new_x <= 0 ) vector.x *= -1;
    };

    this.flyIsRocket = function(){
        var new_x = this.ballXNext();
        var new_y = this.ballYNext();
        this.context.beginPath();
        this.context.rect(this.rocket.x_rock, this.rocket.y_rock, this.width, this.height);
        if( this.context.isPointInPath(new_x, new_y) && !this.rocket.rotate_timerID ){
            vector.y *= -1;
        }
        this.context.closePath();
    };

    this.flyIsRotateRocket = function(){
        var new_x = this.ballXNext();
        var new_y = this.ballYNext();
        this.context.save();
        this.context.translate(this.x_rock, this.y_rock);
        this.context.rotate(Math.PI/180 + this.rotate_count );
        this.context.beginPath();
        this.context.rect(0, 0, this.width, this.height);
        if(this.context.isPointInPath(new_x, new_y)){
            var xa = this.rocket.transformed_coords.x - this.x_rock;
            var ya = this.rocket.transformed_coords.y - this.y_rock;
            var am = Math.sqrt(xa*xa + ya*ya);
            var xb = new_x - this.ball.x_ball;
            var yb = new_y - this.ball.y_ball;
            var bm = Math.sqrt(xb*xb + yb*yb);
            var angle = Math.acos((xa * xb  + ya + yb) / (am * bm));
            var new_angle = 0;
            if(angle < Math.PI / 2){
                new_angle = Math.PI - 2*angle;
            } else {
                new_angle = angle - Math.PI;
            }
            var coords_vec = this.bulletXY(new_x, new_y, this.ball.x_ball, this.ball.y_ball, new_angle);
            vector.x = coords_vec.x - new_x;
            vector.y = coords_vec.y - new_y;
            var len = Math.sqrt(vector.x * vector.x + vector.y * vector.y);
            vector.x *= this.radius / len;
            vector.y *= this.radius / len;
        }
        this.context.closePath();
        this.context.restore();
    };

    this.flyIsBlocks = function(){
        var new_x = this.ballXNext();
        var new_y = this.ballYNext();
        for(var i = 0; i < this.blocks.case.length; i++){
            for(var j = 0; j < this.blocks.case[i].length; j++){
                /**
                 * @type YYO_Block
                 */
                var current_block = this.blocks.case[i][j];
                if(current_block.isPointInside(new_x, new_y)){
                    if(current_block.getX() + current_block.getWidth() === new_x || current_block.getY() + current_block.getHeight() === new_y ||
                        current_block.getX() + current_block.getWidth() === new_x || current_block.getY() === new_y){
                        vector.x *= -1;
                        vector.y *= -1;
                    } else {
                        if (current_block.getY() + current_block.getHeight() < this.y_ball) {
                            vector.y *= -1;
                        } else {
                            if ((current_block.getX() + current_block.getWidth() < this.x_ball ) || (current_block.getX() > this.x_ball)) {
                                vector.x *= -1;
                            } else {
                                vector.y *= -1;
                            }
                        }
                    }
                    if(current_block.getType() === 'expand'){
                        this.blocks.expandBlocksAction();
                    } else if(current_block.getType() === 'catch'){
                        this.blocks.catchBlocksAction();
                    }
                    current_block.selfClear();
                    break;
                }
            }
        }
    };

    this.timer_flyID = 0;

    return this;
}